Hi There! 👋

I'm Kik, and you can find me around tinkering with everything I find. If
you find some toy or tool disassembled somewhere, it's probably me, but
I'll deny everything. :P

If you're looking for my work on GitLab's ActivityPub implementation, here are a few useful links:

- [The design documents with which it all started](https://gitlab.com/oelmekki/gitlab-activitypub-design), in which I designed, planned and specified what the implementation will be.
- [The epic with which GitLab adopted the project](https://gitlab.com/groups/gitlab-org/-/epics/11247). The activities at the bottom of the page are the best way to see what's going on currently, as all discussions and merge requests will appear there. This page is the hub where you can join and discuss if you want to.
- [The work-in-progress documentation](https://docs.gitlab.com/ee/development/activitypub/) (it will be filled as we add more features)

Right now, everything developed is behind a feature flag. It means that even already merged feature can't be used on gitlab.com or on your GitLab self-hosted installation, the flag needs to be turned on (which, for now, is only useful when developing those features, since they are incomplete).

Before going after the holy grail of enabling creating merge requests between GitLab instances, we're starting with something simpler : allowing to publish activities from GitLab to the Fediverse, starting with allowing people from the Fediverse to subscribe to project releases, so they can be warned on their social media feed when their favorite projects make new releases. Such a feed is called "an actor", in ActivityPub terminology, and having such a simple one working will lay the foundation for all the cool features we want to add later.

Want to see actual code? Here are selected past merge requests:

- [The very first one, where we added the first stones: the outbox and the profile endpoints for the releases actor](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/127023)
- [Then we added the database logic for subscriptions](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132889)
- [The Sidekiq workers accepting the subscriptions and fetching data from federated server](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134646)
- [The request handling part of the subscription logic](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135209)
- [The emiting of event when a new release is made](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137253) (not strictly ActivityPub releated, but we needed it to implement the releases actor)
